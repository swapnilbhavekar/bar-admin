function plusminus() {
    $('.minus').click(function() {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function() {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
}

function tab_scroll() {
    $('.scroll-tabs').each(function() {
        var tab_width = 0,
            tab_parent = $(this);
        $(this).find('li').each(function() {
            tab_width += $(this).innerWidth();
        });
        tab_parent.width(tab_width + 'px');
    });
}

function custom_select() {
    $('.select').customSelect();
}

function multiselect() {
    $('.js-example-basic-multiple').select2({
        placeholder: "Select multiple",

    });
}

function auto_suggest() {
    // Initializing the typeahead
    var numbers = ['9867955974', '8082325073', '9897909876', '9909090987', '8909876543', '8898987654', '987654321'];

    // Constructing the suggestion engine
    var numbers = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: numbers
    });

    // Initializing the typeahead
    $('.typeahead').typeahead({
        hint: true,
        highlight: true,
        /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing result */
    }, {
        name: 'numbers',
        source: numbers
    });
    $('.typeahead').attr('autocomplete', 'autocomplete_off_hack_xfr4');
}

function popup_datepicker() {
    var element = $('.upfrontdatepicker');
    var date = new Date();
    element.datepicker({
        inline: true,
        todayHighlight: true,
        format: 'DD/MM/YYYY',
        startDate: date
    }).on('changeDate', function(e) {
        element.prev().val(element.datepicker('getDate'));
    });
}

function form_date_picker() {
    $('.datepicker').datepicker({
        "orientation": 'left'
    });
}

function table_dropdown() {
    $('.dropdown-heading').click(function() {
        if ($(this).parent().hasClass('first-level') == true) {
            if ($(this).hasClass('open') == false) {
                $(this).parent().nextUntil('.first-level', '.second-level').show();
                $(this).addClass('open');
            } else {
                $(this).parent().nextUntil('.first-level').hide();
                $(this).parent().nextUntil('.first-level', '.second-level').find('.dropdown-heading').removeClass('open');
                $(this).removeClass('open');
            }
        } else {
            if ($(this).hasClass('open') == false) {
                $(this).parent().nextUntil('.second-level', '.third-level').show();
                $(this).addClass('open');
            } else {
                $(this).parent().nextUntil('.second-level', '.third-level').hide();
                $(this).removeClass('open');
            }
        }
        // var level = '.first-level';
        // if ($(this).hasClass('first-level') == true) {
        //     level = '.second-level';
        // }
        // if ($(this).hasClass('open') == false) {
        //     $(this).parent().nextUntil('.dropdown-heading-parent', level).show();
        //     $(this).addClass('open');
        // } else {
        //     $(this).parent().nextUntil('.dropdown-heading-parent', level).hide();
        //     $(this).removeClass('open');
        // }
    });
}

function editTextbox() {
    $('.btn-edit').click(function() {
        var currentsection = $(this).parents('.editableWrap');
        if (currentsection.hasClass('editOn')) {
            currentsection.removeClass('editOn');
            currentsection.find('.onClick').attr("readonly", true);
            $(this).text('Edit');
            $('.btn-edit').removeClass('not-active');
        } else {
            currentsection.find('.onClick').attr("readonly", false);
            currentsection.addClass('editOn');
            $(this).text('Save');
            $('.btn-edit').addClass('not-active');
            $(this).removeClass('not-active');;
        }

    });
}

function dropdowntab() {
    $('.dropdownname').on('change', function(e) {
        var current = this.value;
        $('.dropdowntab').hide();
        $('.' + current).fadeIn();
    });
}

function reinitdropdown() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        //var target = $(e.target).attr("href") // activated tab
        //alert(target);
        $('.dropdownname').trigger('render');
    });
    // $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    //     $('.dropdownname').trigger('render');
    // });
}

function side_bar() {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

    trigger.click(function() {
        hamburger_cross();
    });

    function hamburger_cross() {

        if (isClosed == true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function() {
        $('#wrapper').toggleClass('toggled');
    });
}

function feedback() {
    $('.ctm-icons span').on('click', function() {
        var onStar = parseInt($(this).data('value')); // The star currently selected
        var stars = $(this).parent().children('span.icon');
        // $(this).parent().children('span').removeClass('icon-star');
        $(this).parent().next().val(onStar);
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('icon-star-fill');
            $(stars[i]).removeClass('icon-star');
        }
        for (i = 5; i >= onStar; i--) {
            $(stars[i]).addClass('icon-star');
            $(stars[i]).removeClass('icon-star-fill');
        }
    });
}

function report_graph() {
    if ($('#pie-chart').length == 1) {
        Highcharts.chart('pie-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'middle',
                padding: 0,
                itemMarginTop: 5,
                itemMarginBottom: 5,
            },
            colors: ['#91a603', '#020202', '#f40000'],
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    size: '100%',
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Total Counter Year',
                    y: 61.41,
                }, {
                    name: 'Our Capacity',
                    y: 11.84
                }, {
                    name: 'Total Reservation',
                    y: 11.84
                }]
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 320
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        }
                    }
                }]
            }
        });
    }

}

function restaurant() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href") // activated tab
        if (target == "#albums") {
            console.log(target);
            $('#albums .select').trigger('render');
            //$('#albums .select').customSelect();
        }
    });
}

function horizontal_bar() {
    Highcharts.chart('bar-chart', {
        chart: {

            type: 'bar'
        },
        credits: {
            enabled: false
        },
        colors: ['#91a603', 'rgb(244, 0, 0)', 'rgb(2, 2, 2)'],
        title: {

            text: 'Covers by Type'
        },
        xAxis: {
            lineWidth: 0,
            minorGridLineWidth: 14,
            categories: [''],
        },
        yAxis: {
            gridLineWidth: 0,
            visible: false,

            endOnTick: false,
            title: {
                text: ''
            }

        },
        legend: {
            reversed: true,
            layout: 'vertical',
            // width: '300',
            itemMarginBottom: 8,
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        series: [{
            name: '13773 Online Covers',
            data: [9]
        }, {
            name: '60 Phone Covers',
            data: [3]
        }, {
            name: '0 walk-ins',
            data: [2]

        }]
    });
}


function fourfield_chart() {
    Highcharts.chart('bar-chart2', {
        chart: {
            type: 'bar'
        },
        credits: {
            enabled: false
        },
        colors: ['#91a603', 'rgb(244, 0, 0)', 'rgb(2, 2, 2)', ' #fdc9c9'],
        title: {
            text: 'Online Covers'
        },
        xAxis: {

            lineWidth: 0,
            categories: ['']
        },

        yAxis: {
            gridLineWidth: 0,
            visible: false,
            endOnTick: false,
            title: false
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        legend: {
            reversed: true,
            itemMarginBottom: 8,
            // width: 500,
        },

        series: [{
                name: '6322 Your Website',
                data: [8]
            }, {
                name: '5092 OpenTable Direct',
                data: [4]
            }, {
                name: '2359 OpenTables Partners and Affiliates',
                data: [3]
            },
            {
                name: '0 promotions',
                data: [1]
            }
        ]
    });

    $(window).resize(function() {
        var chart = $('#bar-chart2').highcharts();

        console.log('redraw');
        var w = $('#bar-chart2').closest(".wrapper").width()
            // setsize will trigger the graph redraw 
        chart.setSize(
            w, w * (3 / 4), false
        );
    });
}




function counter() {
    $('.counter').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
                countNum: countTo
            },

            {

                duration: 8000,
                easing: 'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }

            });

    });
}

function intel() {
    //var input = document.querySelector("#phone");
    //window.intlTelInput(input);
    $("#phone").intlTelInput({
        separateDialCode: true,
        preferredCountries: ['sa', 'bh', 'ae', 'kw', 'om']

    });
}

$(function() {
    intel();
    plusminus();
    tab_scroll();
    custom_select();
    multiselect();
    form_date_picker();
    popup_datepicker();
    dropdowntab();
    editTextbox();
    reinitdropdown();
    auto_suggest();
    side_bar();
    report_graph();
    table_dropdown();
    restaurant();
    horizontal_bar();
    fourfield_chart();
    counter();
    intinput();
    //$('#addday').modal('show');


    feedback();
});
$(window).on("load", function() {

});

$(window).resize(function() {
    var width;
    if ($(this).width() != width) {
        width = $(this).width();
        // call function is resize here
    }
});